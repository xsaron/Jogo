#ifndef TRAP_HPP
#define TRAP_HPP

#include "game_object.hpp"

class Trap : public Game_object {
public:
    Trap(Map *map);
    ~Trap();
    void move(Map *map);
};

#endif
