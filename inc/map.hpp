#ifndef MAP_HPP
#define MAP_HPP

class Map {
private:
    char map[20][50];
public:
    Map();
    ~Map();
    void read_map();
    void set_character(char *sprite, int position_x, int position_y);
    char get_character(int position_y, int position_x);
};

#endif
