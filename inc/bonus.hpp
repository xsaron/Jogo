#ifndef BONUS_HPP
#define BONUS_HPP

#include "game_object.hpp"

class Bonus : public Game_object {
public:
    Bonus(Map *map);
    ~Bonus();
};

#endif
