#ifndef DRAW_HPP
#define DRAW_HPP
#include "map.hpp"
#include "trap.hpp"

class Draw {
public:
    Draw();
    void draw_map(Map *map);
    void draw_player(Map *map, char *sprite, int position_x, int position_y);
    void draw_trap(Map *map, char *sprite, int position_x, int position_y);
    void draw_bonus(Map *map, char *sprite, int position_x, int position_y);
    void draw_rank();
};

#endif
