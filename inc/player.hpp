#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "game_object.hpp"
#include "bonus.hpp"
#include "trap.hpp"

class Player : public Game_object {
private:
    bool alive;
    int score;
    int lifes;
    bool winner;
public:
    Player();
    ~Player();
    bool get_alive();
    void set_alive(bool alive);
    int get_score();
    void set_score(int score);
    int get_lifes();
    void set_lifes(int life);
    bool get_winner();
    void set_winner(bool winner);
    void add_score(Bonus *bonus);
    void lose_life(Trap *trap);
    void win(Map *map);
};

#endif
