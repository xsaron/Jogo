#ifndef RANK_HPP
#define RANK_HPP

#include <string>
#include "player.hpp"

using namespace std;

class Rank {
private:
    string name;
    int score;
public:
    Rank();
    ~Rank();
    void print_rank();
    void write_rank(Player *player);
    string get_name();
    void set_name(string name);
    int get_score();
    void set_score(int score);


};

#endif
