#ifndef GAME_OBJECT_HPP
#define GAME_OBJECT_HPP
#include "map.hpp"

class Game_object{
private:
    int position_x;
    int position_y;
    char* sprite;
public:
    // Game_object();
    // ~Game_object();
    int get_position_x();
    void set_position_x(int position_x);
    int get_position_y();
    void set_position_y(int position_y);
    char *get_sprite();
    void set_sprite(char *sprite);
    virtual void move(char key, Map *map);
    

};

#endif
