#include "bonus.hpp"
#include "game_object.hpp"
#include "map.hpp"
#include <stdlib.h>
#include <ncurses.h>
#include <ctime>

Bonus::Bonus(Map *map) {
    set_sprite("$");

    set_position_y(rand() % 20);
    set_position_x(rand() % 50);

    while(map->get_character(get_position_y(), get_position_x()) == '=') {
        set_position_y(rand() % 20);
        set_position_x(rand() % 50);
    }
}
Bonus::~Bonus() {
    set_sprite("-");
}
