#include <ncurses.h>
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <ncurses.h>
#include <ctime>
#include <iomanip>
#include "map.hpp"
#include "game_object.hpp"
#include "player.hpp"
#include "draw.hpp"
#include "trap.hpp"
#include "bonus.hpp"
#include "rank.hpp"

// #define num_trap 30
// #define num_bonus 10

using namespace std;

int get_difficulty(int option);

int main(int argc, char const *argv[]) {

    printf("Escolha a dificuldade:\n");
    printf("1 - Fácil\n");
    printf("2 - Médio\n");
    printf("3 - Difícil\n");
    int option = getchar();
    int num_bonus = 2*get_difficulty(option);
    int num_trap = 280/get_difficulty(option);

    initscr();
    srand(time(NULL));
    clear();
    noecho();


    /* Creates the map object, reads the .txt file and prints the map */
    Map *map_1 = new Map();
    map_1->read_map();
    Draw *draw = new Draw();

    /* Creates the 'rank' object */
    Rank *rank = new Rank();

    /* Creates a list of traps */
    Trap *trap_list[num_trap];
    for(int i = 0; i < num_trap; i++) {
        trap_list[i] = new Trap(map_1);
    }

    /* Creates the bonus objects */
    Bonus *bonus_list[num_bonus];
    for(int i = 0; i < num_bonus; i++) {
        bonus_list[i] = new Bonus(map_1);
    }
    for(int i = 0; i < num_bonus; i++) {
        draw->draw_bonus(map_1, bonus_list[i]->get_sprite(), bonus_list[i]->get_position_x(), bonus_list[i]->get_position_y());
    }

    /* Creates the player object, draws the 'first player' and then, draws the map */
    Player *player = new Player();
    // int num_bonus = player->get_difficulty(option);
    draw->draw_player(map_1, player->get_sprite(), player->get_position_x(), player->get_position_y());

    draw->draw_map(map_1);
    printw("Bufunfa: %d------", player->get_score());
    printw("Mentiras: %d", player->get_lifes());

    while(player->get_alive() == 1 && player->get_winner() == FALSE) {

        /* Gets the moviment key, moves the player, reads the map again,
        adds the 'player' sprite and prints the map again */
        char key = getch();
        player->move(key, map_1);
        map_1->read_map();

        /* Tests if 'win' is true */
        player->win(map_1);

        /* Draws all bonus objects into the map */
        for(int i = 0; i < num_bonus; i++) {
            draw->draw_bonus(map_1, bonus_list[i]->get_sprite(), bonus_list[i]->get_position_x(), bonus_list[i]->get_position_y());
            player->add_score(bonus_list[i]);
            if(player->get_position_x() == bonus_list[i]->get_position_x() && player->get_position_y() == bonus_list[i]->get_position_y()) {
                bonus_list[i]->~Bonus();
                bonus_list[i] = new Bonus(map_1);
            }
        }
        /* Creates all traps objects and draw the traps in the map */
        for(int i = 0; i < num_trap; i++) {
            trap_list[i]->move(map_1);
            draw->draw_trap(map_1, trap_list[i]->get_sprite(), trap_list[i]->get_position_x(), trap_list[i]->get_position_y());
            player->lose_life(trap_list[i]);
            trap_list[i]->~Trap();
        }

        /* Draws the player into the map */
        draw->draw_player(map_1, player->get_sprite(), player->get_position_x(), player->get_position_y());

        refresh();
        clear();

        if (player->get_winner() == TRUE || player->get_alive() == FALSE) {
            system("clear");
            printf("---GAME OVER---");
        }
        /* Draws the final map, with all the game objects */
        draw->draw_map(map_1);
        printw("Bufunfa: %d------", player->get_score());
        printw("Mentiras: %d", player->get_lifes());
        if(player->get_lifes() <= 0) {
            player->set_alive(0);
        }

    }

    endwin();
    clear();

    initscr();
    system("clear");
    rank->write_rank(player);
    system("clear");
    rank->print_rank();
    endwin();

    return 0;
}

int get_difficulty(int option) {
    int constant;

    switch(option) {
        case '1':
            /* Easy */
            constant = 20;
            break;
        case '2':
            /* Medium */
            constant = 10;
            break;
        case '3':
            /* Hard */
            constant = 5;
            break;
    }
    return constant;
}
