#include "draw.hpp"
#include "map.hpp"
#include "trap.hpp"
#include "ncurses.h"

Draw::Draw() {}

void Draw::draw_map(Map *map) {
    for(int i = 0; i < 20; i++) {
        for(int j = 0; j < 50; j++) {
            printw("%c", map->get_character(i, j));
            if(j >= 49){
                printw("\n");
            }
        }
    }
}

void Draw::draw_player(Map *map, char *sprite, int position_x, int position_y) {
    map->set_character(sprite, position_x, position_y);
}

void Draw::draw_trap(Map *map, char *sprite, int position_x, int position_y) {
    map->set_character(sprite, position_x, position_y);
}

void Draw::draw_bonus(Map *map, char *sprite, int position_x, int position_y) {
    map->set_character(sprite, position_x, position_y);
}
