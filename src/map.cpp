#include "map.hpp"
#include <fstream>
#include <string>
#include <ncurses.h>
using namespace std;

Map::Map() {}

void Map::read_map() {
    ifstream map("./doc/map.txt");
    string line;

    for(int i = 0; i < 20; i++) {
        getline(map, line);
        for(int j = 0; j < 50; j++) {
            this->map[i][j] = line[j];
        }
    }
    map.close();
}

char Map::get_character(int position_y, int position_x) {
    return this->map[position_y][position_x];
}

void Map::set_character(char *sprite, int position_x, int position_y) {
    this->map[position_y][position_x] = *sprite;
}
