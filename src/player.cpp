#include "game_object.hpp"
#include "player.hpp"
#include "map.hpp"
#include <ncurses.h>
#define WIN_SCORE 5

Player::Player() {
    set_position_x(1);
    set_position_y(1);
    set_sprite("@");
    set_alive(true);
    set_score(0);
    set_lifes(3);
    set_winner(FALSE);
}

bool Player::get_alive() {
    return alive;
}
void Player::set_alive(bool alive) {
    this->alive = alive;
}

int Player::get_score() {
    return score;
}
void Player::set_score(int score) {
    this->score = score;
}

int Player::get_lifes() {
    return lifes;
}
void Player::set_lifes(int lifes) {
    this->lifes = lifes;
}

bool Player::get_winner() {
    return winner;
}
void Player::set_winner(bool winner) {
    this->winner = winner;
}

void Game_object::move(char key, Map *map) {

    switch(key) {
        case 'w':
            /* If the character next to the is a wall, don't move it */
            if(map->get_character(get_position_y()-1, get_position_x()) == '=') {
                set_position_y(get_position_y());
            } else {
                set_position_y(get_position_y()-1);
            }
            break;
        case 's':
            if(map->get_character(get_position_y()+1, get_position_x()) == '=') {
                set_position_y(get_position_y());
            } else {
                set_position_y(get_position_y()+1);
            }
            break;
        case 'd':
            if(map->get_character(get_position_y(), get_position_x()+1) == '=') {
                set_position_x(get_position_x());
            } else {
                set_position_x(get_position_x()+1);
            }
            break;
        case 'a':
            if(map->get_character(get_position_y(), get_position_x()-1) == '=') {
                set_position_y(get_position_y());
            } else {
                set_position_x(get_position_x()-1);
            }
            break;
    }

}

void Player::add_score(Bonus *bonus) {
    if(get_position_x() == bonus->get_position_x() && get_position_y() == bonus->get_position_y()) {
        set_score(get_score() + 1);
    }
}

void Player::lose_life(Trap *trap) {
    if(get_position_x() == trap->get_position_x() && get_position_y() == trap->get_position_y()) {
        set_lifes(get_lifes() - 1);
    }
}

void Player::win(Map *map) {
    if(map->get_character(get_position_y(), get_position_x()) == '8' && get_score() >= WIN_SCORE) {
        set_winner(TRUE);
    }
}
