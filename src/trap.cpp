#include "trap.hpp"
#include "game_object.hpp"
#include "map.hpp"
#include <stdlib.h>
#include <ncurses.h>
#include <ctime>

Trap::Trap(Map *map) {
    set_sprite("%");

    set_position_y(rand() % 20);
    set_position_x(rand() % 50);

    while(map->get_character(get_position_y(), get_position_x()) == '=') {
        set_position_y(rand() % 20);
        set_position_x(rand() % 50);
    }
}

Trap::~Trap() {}

void Trap::move(Map *map) {

    int option = 1 + (rand() % (4 - 1 + 1));
    
    if(option == 1) {
            if (map->get_character(get_position_y() + 1, get_position_x()) != '=') {
                set_position_y(get_position_y() + 1);
            } else if (map->get_character(get_position_y(), get_position_x() + 1) != '=') {
                set_position_x(get_position_x() + 1);
            } else if (map->get_character(get_position_y() - 1, get_position_x()) != '=') {
                set_position_y(get_position_y() - 1);
            } else if (map->get_character(get_position_y(), get_position_x() - 1) != '=') {
                set_position_x(get_position_x() - 1);
            }
    }
    if(option == 2) {
            if (map->get_character(get_position_y(), get_position_x() + 1) != '=') {
                set_position_x(get_position_x() + 1);
            } else if (map->get_character(get_position_y() - 1, get_position_x()) != '=') {
                set_position_y(get_position_y() - 1);
            } else if (map->get_character(get_position_y(), get_position_x() - 1) != '=') {
                set_position_x(get_position_x() - 1);
            } else if (map->get_character(get_position_y() + 1, get_position_x()) != '=') {
                set_position_y(get_position_y() + 1);
            }
    }

        if(option == 3) {
            if (map->get_character(get_position_y() - 1, get_position_x()) != '=') {
                set_position_y(get_position_y() - 1);
            } else if (map->get_character(get_position_y(), get_position_x() - 1) != '=') {
                set_position_x(get_position_x() - 1);
            } else if (map->get_character(get_position_y() + 1, get_position_x()) != '=') {
                set_position_y(get_position_y() + 1);
            } else if (map->get_character(get_position_y(), get_position_x() + 1) != '=') {
                set_position_x(get_position_x() + 1);
            }
        }

        if(option == 4) {
            if (map->get_character(get_position_y(), get_position_x() - 1) != '=') {
                set_position_x(get_position_x() - 1);
            } else if (map->get_character(get_position_y() + 1, get_position_x()) != '=') {
                set_position_y(get_position_y() + 1);
            } else if (map->get_character(get_position_y(), get_position_x() + 1) != '=') {
                set_position_x(get_position_x() + 1);
            } else if (map->get_character(get_position_y() - 1, get_position_x()) != '=') {
                set_position_y(get_position_y() - 1);
            }
        }
    }


    // if (map->get_character(get_position_y() + 1, get_position_x()) != '=') {
    //     set_position_y(get_position_y() + 1);
    // } else if (map->get_character(get_position_y() - 1, get_position_x()) != '=') {
    //     set_position_y(get_position_y() - 1);
    // } else if (map->get_character(get_position_y(), get_position_x() + 1) != '=') {
    //     set_position_x(get_position_x() + 1);
    // } else if (map->get_character(get_position_y(), get_position_x() - 1) != '=') {
    //     set_position_x(get_position_x() - 1);
    // }
