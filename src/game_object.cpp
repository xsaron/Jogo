#include "game_object.hpp"
#include <cstdlib>
#include <iostream>

using namespace std;

int Game_object::get_position_x() {
    return position_x;
}
void Game_object::set_position_x(int position_x) {
    this->position_x = position_x;
}

int Game_object::get_position_y() {
    return position_y;
}
void Game_object::set_position_y(int position_y) {
    this->position_y = position_y;
}

char *Game_object::get_sprite() {
    return sprite;
}
void Game_object::set_sprite(char *sprite) {
    this->sprite = sprite;
}
