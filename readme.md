# Making some money
---
**Making some money** happens in Brazil. A walking politician acting like normal people, but stealing and lying to people all the time. Don't fool yourself: you're the politician.
---
# How can I play this amazing game?

### It's simple. All you have to do is install the **ncurses** library:
---
    * As a user of __Debian based systems__, write this on your terminal: **sudo apt-get install libncurses5-dev libncursesw5-dev**
    * As a user of __Mac OS__, refer to this link: https://gist.github.com/cnruby/960344
    * As a user of __Windows__, please get a Debian based system or a Mac OS. Sorry.
---
# How to play
---
    * To run the game, you have to execute the **msm** file. For Unix systems, type __./msm__ on your terminal to run the game.
    * As the game starts, you will have to choose the difficult of the game and there are only three options: easy, medium and hard.
    * Make sure you choose it right because at the end of the game, there is a **ranking** of the best players ever.
    * When the game ends (either you died or won the game), you will type your name to be recorded on the rank.
---
# The Game
---
    You are a politician, so you have to steal money and lie to the citizens.
    '@' Represents you, the politician.
    '%' Represents all of the citizens. **Be careful, you can only lie three times. They are not so foolish.**
    '$' Represents the money you have to steal.
    '=' Are walls, you can't go through it.
    Remember: you are greedy, so never stop stealing from this foolish people. The more you steal, the most you'll be remembered.
